package com.example.mememaker;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.w3c.dom.Text;

import java.util.concurrent.locks.Lock;

public class AddMemeFragment extends Fragment {

    ImageView imageView;
    Button buttonChooseMeme, buttonSaveText, buttonUploadMeme;
    TextView topTextView, bottomTextView;
    EditText topEditText, bottomEditText;

    Uri imageUri;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_meme, container, false);

        imageView = (ImageView) view.findViewById(R.id.chosen_meme_image);

        topTextView = (TextView) view.findViewById(R.id.top_text_view);
        bottomTextView = (TextView) view.findViewById(R.id.bottom_text_view);

        topEditText = (EditText) view.findViewById(R.id.top_edit_text);
        bottomEditText = (EditText) view.findViewById(R.id.bottom_edit_text);
        topEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                topTextView.setText(s);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        bottomEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count){
                bottomTextView.setText(s);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        buttonChooseMeme = (Button) view.findViewById(R.id.choose_meme_button);
        buttonSaveText = (Button) view.findViewById(R.id.clear_text_button);
        buttonUploadMeme = (Button) view.findViewById(R.id.upload_meme);

//        buttonSaveText.setEnabled(false);
//        buttonUploadMeme.setEnabled(false);

        buttonChooseMeme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        buttonSaveText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topTextView.setText(topEditText.getText().toString());
                bottomTextView.setText(bottomEditText.getText().toString());

                topEditText.setText("");
                bottomEditText.setText("");
            }
        });

        buttonUploadMeme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return view;
    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        galleryLauncher.launch(gallery);
    }

    ActivityResultLauncher<Intent> galleryLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) throws IllegalArgumentException {
                    Intent data = result.getData();
                    if (data != null) {
                        imageUri = data.getData();
                        imageView.setImageURI(imageUri);
                    } else {
                        Toast.makeText(getContext(), getString(R.string.failed_to_pick_meme), Toast.LENGTH_SHORT).show();
                    }
                }
            });
}
