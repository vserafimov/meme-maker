package com.example.mememaker;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Objects;

public class AllMemesFragment extends Fragment implements MyRecyclerViewAdapter.ItemClickListener {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_all_memes, container, false);
        MyRecyclerViewAdapter adapter;

        //data to populate the RecyclerView with
        int[] data = {R.drawable.image1, R.drawable.image2, R.drawable.image3, R.drawable.image4,
                R.drawable.image5, R.drawable.image6, R.drawable.image7, R.drawable.image8,
                R.drawable.image9, R.drawable.image10, R.drawable.image11, R.drawable.image12,
                R.drawable.image13, R.drawable.image14};

        //set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.rvAllMemes);
        final int NUMBER_OF_COLUMNS = 2;
        recyclerView.setLayoutManager(new GridLayoutManager(this.getContext(), NUMBER_OF_COLUMNS));
        adapter = new MyRecyclerViewAdapter(this.getContext(), data);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onItemClick(int imageId) {
        //button to expand photo
        FragmentTransaction frTransaction = requireActivity().getSupportFragmentManager().beginTransaction();
        frTransaction.replace(R.id.fragment_container, FullSizeImageFragment.getInstance(imageId));
        frTransaction.commit();
    }
}
