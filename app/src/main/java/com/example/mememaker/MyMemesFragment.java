package com.example.mememaker;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MyMemesFragment extends Fragment implements MyRecyclerViewAdapter.ItemClickListener {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_memes, container, false);
        MyRecyclerViewAdapter adapter;

        //data to populate the RecyclerView with
        int[] data = {R.drawable.image11, R.drawable.image4, R.drawable.image9, R.drawable.image8,
                R.drawable.image12};

        //set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.rvMyMemes);
        final int NUMBER_OF_COLUMNS = 2;
        recyclerView.setLayoutManager(new GridLayoutManager(this.getContext(), NUMBER_OF_COLUMNS));
        adapter = new MyRecyclerViewAdapter(this.getContext(), data);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onItemClick(int imageId) {

    }
}
