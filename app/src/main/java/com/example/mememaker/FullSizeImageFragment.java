package com.example.mememaker;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class FullSizeImageFragment extends Fragment {
    private static final String ARGUMENT_IMAGE_RES = "ARGUMENT_IMAGE_RES";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fullsize_image, container, false);
        Bundle arguments = requireArguments();
        int imageDrawableResId = arguments.getInt(ARGUMENT_IMAGE_RES);
        ImageView imageView = (ImageView) view.findViewById(R.id.iv_full_size_image);

        imageView.setImageResource(imageDrawableResId);

        return view;
    }

    public static FullSizeImageFragment getInstance(@DrawableRes int imageId) {
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_IMAGE_RES, imageId);
        FullSizeImageFragment fragment = new FullSizeImageFragment();
        fragment.setArguments(arguments);

        return fragment;
    }
}
